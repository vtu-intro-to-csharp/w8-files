﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignment4_Students
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string path = "students.txt";
            WriteStudentsData(path);
            PrintStudentsData(path);
        }

        static void WriteStudentsData(string path)
        {
            Console.Write("Enter number of students: ");
            int count = int.Parse(Console.ReadLine());
            Student[] students = new Student[count];

            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine("Enter student name: ");
                string name = Console.ReadLine();

                Console.WriteLine("Enter student ID number: ");
                string idNumber = Console.ReadLine();

                Console.WriteLine("Enter student average grade: ");
                double grade = double.Parse(Console.ReadLine());

                Student student = new Student(name, idNumber, grade);
                students[i] = student;
            }

            using(StreamWriter writer = new StreamWriter(path))
            {
                for (int i = 0; i < students.Length; i++)
                {
                    writer.WriteLine(students[i].Name);
                    writer.WriteLine(students[i].IdNumber);
                    writer.WriteLine(students[i].Grade);
                }    
            }
        }

        static void PrintStudentsData(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            using (StreamReader reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    Console.WriteLine(reader.ReadLine());
                    reader.ReadLine();
                    Console.WriteLine(reader.ReadLine()); 
                }
            }
        }
    }
}