﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4_Students
{
    public class Student
    {
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public double Grade { get; set; }

        public Student(string name, string idNumber, double grade)
        {
            Name = name;
            IdNumber = idNumber;
            Grade = grade;
        }
    }
}
