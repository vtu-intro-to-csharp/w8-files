﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace FilesDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string fullPath = @"C:\Users\petar.yochev\source\repos\w8-files\FilesDemo\file.txt";
            string relativePath = @"..\..\..\file.txt";

            //за големи файлове
            using (StreamWriter writer = new StreamWriter(relativePath))
            {
                for (int i = 0; i < 6; i++)
                {
                    writer.WriteLine(i);
                }
            }

            //за големи файлове
            using (StreamReader reader = new StreamReader(relativePath))
            {
                while (!reader.EndOfStream)
                {
                    Console.WriteLine(reader.ReadLine()); 
                }
            }

            //за малки файлове
            File.WriteAllText(relativePath, "Hello World!\n");

            //за малки файлове
            Console.WriteLine(File.ReadAllText(relativePath));


            //допълване съдържанието на файл
            FileStream fileStream = File.Open(relativePath, FileMode.Append);
            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                writer.WriteLine("What's going on?");
            }
        }
    }
}