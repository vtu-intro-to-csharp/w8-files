﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExceptionsDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string relativePath = @"..\..\..\file.txt";

            try
            {
                using (StreamReader reader = new StreamReader(relativePath))
                {
                    while (!reader.EndOfStream)
                    {
                        Console.WriteLine(reader.ReadLine());
                    }
                }
            }
            catch (FileNotFoundException exception)
            {
                Console.WriteLine(exception.Message);                
            }

            Console.WriteLine("End of program");
        }
    }
}